from locust import HttpLocust, TaskSet


def index(l):
    l.client.get("/")


def rest(l):
    l.client.get("/rest")


def diagram(l):
    l.client.get("/diagram")


def register(l):
    l.client.get("/register")


def scope(l):
    l.client.get("/scope")


class UserBehavior(TaskSet):
    tasks = {index: 1, rest: 1, diagram: 1, scope: 1}

    def on_start(self):
        self.login()

    def login(self):
        response = self.client.get("/login/")
        csrftoken = response.cookies['csrftoken']
        response = self.client.post("/login/",
                                    {"username": "celina",
                                     "password": "aga123"},
                                    headers={"X-CSRFToken": csrftoken})


class WebsiteUser(HttpLocust):
    host = 'http://localhost:8000'
    task_set = UserBehavior
    max_wait = 9000
