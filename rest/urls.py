from django.conf.urls import include, url
from .views import Rest_beacon, Rest_information, Rest_info_beacon

urlpatterns = [
    url(r'^$', Rest_beacon.as_view(), name='restBeacon'),
    url(r'^info/$', Rest_information.as_view(), name='restInformation'),
    url(r'^info/(?P<pk>\d+)/$', Rest_info_beacon.as_view(), name='restInfoBeacon')
]