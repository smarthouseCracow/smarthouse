from rest_framework import serializers
from beacon.models import Beacon
from information.models import Information


class BeaconSerializer(serializers.ModelSerializer):
    class Meta:
        model = Beacon
        fields = ('id', 'name', 'location', 'number')


class InformationSerializer(serializers.ModelSerializer):
    id_beacon = serializers.StringRelatedField()
    image = serializers.ImageField(max_length=None, use_url=True,)

    class Meta:
        model = Information
        fields = ('id', 'name', 'image', 'content', 'data', 'id_beacon')