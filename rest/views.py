from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.views import APIView
from beacon.models import Beacon
from .serializers import BeaconSerializer, InformationSerializer
from information.models import Information


class Rest_beacon(APIView):
    def get(self, request, format=None):
        beacons = Beacon.objects.all()
        serializer = BeaconSerializer(beacons, many=True)
        return Response(serializer.data)


class Rest_information(APIView):
    def get(self, request, format=None):
        informations = Information.objects.all()
        serializer = InformationSerializer(informations, many=True)
        return Response(serializer.data)


class Rest_info_beacon(APIView):
    def get(self, request, pk, format=None):
        info = get_object_or_404(Information, id=pk)
        serializer = InformationSerializer(info)
        return Response(serializer.data)