from django import test
from selenium.webdriver.firefox import webdriver
from create_session_cookie import create_session_cookie


class BasicTestWithSelenium(test.LiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        cls.selenium = webdriver.WebDriver()
        super(BasicTestWithSelenium, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        super(BasicTestWithSelenium, cls).tearDownClass()
        cls.selenium.quit()

    def login_sessions(self, url):
        session_cookie = create_session_cookie(
            username='celina', password='aga123'
        )

        self.selenium.get('%s%s' % (self.live_server_url, url))

        self.selenium.add_cookie(session_cookie)

        self.selenium.refresh()

    def test_home_page(self):
        self.login_sessions('/')
        self.selenium.get('%s%s' % (self.live_server_url, '/'))
        self.selenium.find_element_by_id("23.0")

    def test_login(self):
        self.login_sessions('/login/')
        self.selenium.get('%s%s' % (self.live_server_url, '/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('celina')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('aga123')
        self.selenium.find_element_by_xpath('//input[@value="Zaloguj"]').click()
        self.selenium.find_element_by_id("23.0")

    def test_diagram(self):
        self.login_sessions('/diagram/')
        self.selenium.find_element_by_id('chart_div')

    def test_scope(self):
        self.login_sessions('/scope/')

    def test_contact(self):
        self.login_sessions('/contact/')
        subject_input = self.selenium.find_element_by_name('subject')
        subject_input.send_keys('Dlaczego mi nie dziala')
        emaila_input = self.selenium.find_element_by_name('email2')
        emaila_input.send_keys('cosik@cosik.pl')
        message_input = self.selenium.find_element_by_name('message')
        message_input.send_keys('ocsiicd csdvsvkvd vdfvdf')
        self.selenium.find_element_by_xpath('//input[@value="Wyslij"]').click()

    def test_rest(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/rest/'))

    def test_register(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/register/'))
        username_input = self.selenium.find_element_by_name('username')
        username_input.send_keys('zosia')
        email_input = self.selenium.find_element_by_name('email')
        email_input.send_keys('cosik@cosik.pl')
        password1_input = self.selenium.find_element_by_name('password1')
        password1_input.send_keys('cosikcosik')
        password2_input = self.selenium.find_element_by_name('password2')
        password2_input.send_keys('cosikcosik')
        self.selenium.find_element_by_xpath('//input[@value="Zarejestruj"]').click()


        self.selenium.get('%s%s' % (self.live_server_url, '/login/'))
        username_input = self.selenium.find_element_by_name("username")
        username_input.send_keys('zosia')
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('cosikcosik')
        self.selenium.find_element_by_xpath('//input[@value="Zaloguj"]').click()
